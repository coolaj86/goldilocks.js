# API
The API system is intended for use with Desktop and Mobile clients.
It must be accessed using one of the following domains as the Host header:

* localhost.alpha.daplie.me
* localhost.admin.daplie.me
* alpha.localhost.daplie.me
* admin.localhost.daplie.me
* localhost.daplie.invalid

All requests require an OAuth3 token in the request headers.

## Tokens

Some of the functionality of goldilocks requires the use of OAuth3 tokens to
perform tasks like setting DNS records. Management of these tokens can be done
using the following APIs.

### Get A Single Token
  * **URL** `/api/goldilocks@daplie.com/tokens/:id`
  * **Method** `GET`
  * **Reponse**: The token matching the specified ID. Has the following properties.
    * `id`: The hash used to identify the token. Based on several of the fields
      inside the decoded token.
    * `provider_uri`: The URI for the one who issued the token. Should be the same
      as the `iss` field inside the decoded token.
    * `client_uri`: The URI for the app authorized to use the token. Should be the
      same as the `azp` field inside the decoded token.
    * `scope`: The list of permissions granted by the token. Should be the same
      as the `scp` field inside the decoded token.
    * `access_token`: The encoded JWT.
    * `token`: The decoded token.

### Get All Tokens
  * **URL** `/api/goldilocks@daplie.com/tokens`
  * **Method** `GET`
  * **Reponse**: An array of the tokens stored. Each item looks the same as if it
    had been requested individually.

### Save New Token
  * **URL** `/api/goldilocks@daplie.com/tokens`
  * **Method** `POST`
  * **Body**: An object similar to an OAuth3 session used by the javascript
    library. The only important fields are `refresh_token` or `access_token`, and
    `refresh_token` will be used before `access_token`. (This is because the
    `access_token` usually expires quickly, making it meaningless to store.)
  * **Reponse**: The response looks the same as a single GET request.

### Delete Token
  * **URL** `/api/goldilocks@daplie.com/tokens/:id`
  * **Method** `DELETE`
  * **Reponse**: Either `{"success":true}` or `{"success":false}`, depending on
    whether the token was present before the request.

## Config

### Get All Settings
  * **URL** `/api/goldilocks@daplie.com/config`
  * **Method** `GET`
  * **Reponse**: The JSON representation of the current config. See the [README.md](/README.md)
    for the structure of the config.

### Get Group Setting
  * **URL** `/api/goldilocks@daplie.com/config/:group`
  * **Method** `GET`
  * **Reponse**: The sub-object of the config relevant to the group specified in
    the url (ie http, tls, tcp, etc.)

### Get Group Module List
  * **URL** `/api/goldilocks@daplie.com/config/:group/modules`
  * **Method** `GET`
  * **Reponse**: The list of modules relevant to the group specified in the url
    (ie http, tls, tcp, etc.)

### Get Specific Module
  * **URL** `/api/goldilocks@daplie.com/config/:group/modules/:modId`
  * **Method** `GET`
  * **Reponse**: The module with the specified module ID.

### Get Domain Group
  * **URL** `/api/goldilocks@daplie.com/config/domains/:domId`
  * **Method** `GET`
  * **Reponse**: The domains specification with the specified domains ID.

### Get Domain Group Modules
  * **URL** `/api/goldilocks@daplie.com/config/domains/:domId/modules`
  * **Method** `GET`
  * **Reponse**: An object containing all of the relevant modules for the group
    of domains.

### Get Domain Group Module Category
  * **URL** `/api/goldilocks@daplie.com/config/domains/:domId/modules/:group`
  * **Method** `GET`
  * **Reponse**: A list of the specific category of modules for the group of domains.

### Get Specific Domain Group Module
  * **URL** `/api/goldilocks@daplie.com/config/domains/:domId/modules/:group/:modId`
  * **Method** `GET`
  * **Reponse**: The module with the specified module ID.


### Change Settings
  * **URL** `/api/goldilocks@daplie.com/config`
  * **URL** `/api/goldilocks@daplie.com/config/:group`
  * **Method** `POST`
  * **Body**: The changes to be applied on top of the current config. See the
    [README.md](/README.md) for the settings. If modules or domains are specified
    they are added to the current list.
  * **Reponse**: The current config. If the group is specified in the URL it will
    only be the config relevant to that group.

### Add Module
  * **URL** `/api/goldilocks@daplie.com/config/:group/modules`
  * **URL** `/api/goldilocks@daplie.com/config/domains/:domId/modules/:group`
  * **Method** `POST`
  * **Body**: The module to be added. Can also be provided an array of modules
    to add multiple modules in the same request.
  * **Reponse**: The current list of modules.

### Add Domain Group
  * **URL** `/api/goldilocks@daplie.com/config/domains`
  * **Method** `POST`
  * **Body**: The domains names and modules for the new domain group(s).
  * **Reponse**: The current list of domain groups.


### Edit Module
  * **URL** `/api/goldilocks@daplie.com/config/:group/modules/:modId`
  * **URL** `/api/goldilocks@daplie.com/config/domains/:domId/modules/:group/:modId`
  * **Method** `PUT`
  * **Body**: The new parameters for the module.
  * **Reponse**: The editted module.

### Edit Domain Group
  * **URL** `/api/goldilocks@daplie.com/config/domains/:domId`
  * **Method** `PUT`
  * **Body**: The new domains names for the domains group. The module list cannot
    be editted through this route.
  * **Reponse**: The editted domain group.


### Remove Module
  * **URL** `/api/goldilocks@daplie.com/config/:group/modules/:modId`
  * **URL** `/api/goldilocks@daplie.com/config/domains/:domId/modules/:group/:modId`
  * **Method** `DELETE`
  * **Reponse**: The list of modules.

### Remove Domain Group
  * **URL** `/api/goldilocks@daplie.com/config/domains/:domId`
  * **Method** `DELETE`
  * **Reponse**: The list of domain groups.


## Socks5 Proxy

### Check Status
  * **URL** `/api/goldilocks@daplie.com/socks5`
  * **Method** `GET`
  * **Response**: The returned object will have up to two values inside
    * `running`: boolean value to indicate if the proxy is currently active
    * `port`: if the proxy is running this is the port it's running on

### Start Proxy
  * **URL** `/api/goldilocks@daplie.com/socks5`
  * **Method** `POST`
  * **Response**: Same response as for the `GET` request

### Stop Proxy
  * **URL** `/api/goldilocks@daplie.com/socks5`
  * **Method** `DELETE`
  * **Response**: Same response as for the `GET` request
