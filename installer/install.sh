#!/bin/bash

set -e
set -u


### IMPORTANT ###
###  VERSION  ###
my_name=goldilocks
my_app_pkg_name=com.coolaj86.goldilocks.web
my_app_ver="v1.1"
my_azp_oauth3_ver="v1.2.3"
export NODE_VERSION="v8.9.3"

if [ -z "${my_tmp-}" ]; then
  my_tmp="$(mktemp -d)"
  mkdir -p $my_tmp/opt/$my_name/lib/node_modules/$my_name
  echo "Installing to $my_tmp (will be moved after install)"
  git clone ./ $my_tmp/opt/$my_name/lib/node_modules/$my_name
  pushd $my_tmp/opt/$my_name/lib/node_modules/$my_name
fi

#################
export NODE_PATH=$my_tmp/opt/$my_name/lib/node_modules
export PATH=$my_tmp/opt/$my_name/bin/:$PATH
export NPM_CONFIG_PREFIX=$my_tmp/opt/$my_name
my_npm="$NPM_CONFIG_PREFIX/bin/npm"
#################


my_app_dist=$my_tmp/opt/$my_name/lib/node_modules/$my_name/dist
installer_base="https://git.coolaj86.com/coolaj86/goldilocks.js/raw/$my_app_ver"

# Backwards compat
# some scripts still use the old names
my_app_dir=$my_tmp
my_app_name=$my_name



git checkout $my_app_ver

mkdir -p "$my_tmp/opt/$my_name"/{lib,bin,etc}
ln -s ../lib/node_modules/$my_name/bin/$my_name.js $my_tmp/opt/$my_name/bin/$my_name
ln -s ../lib/node_modules/$my_name/bin/$my_name.js $my_tmp/opt/$my_name/bin/$my_name.js
mkdir -p "$my_tmp/etc/$my_name"
chmod 775 "$my_tmp/etc/$my_name"
cat "$my_app_dist/etc/$my_name/$my_name.example.yml" > "$my_tmp/etc/$my_name/$my_name.example.yml"
chmod 664 "$my_tmp/etc/$my_name/$my_name.example.yml"
mkdir -p $my_tmp/srv/www
mkdir -p $my_tmp/var/www
mkdir -p $my_tmp/var/log/$my_name



#
# Helpers
#
source ./installer/sudo-cmd.sh
source ./installer/http-get.sh



#
# Dependencies
#
echo $NODE_VERSION > /tmp/NODEJS_VER
http_bash "https://git.coolaj86.com/coolaj86/node-installer.sh/raw/v1.1/install.sh"
$my_npm install -g npm@4
pushd $my_tmp/opt/$my_name/lib/node_modules/$my_name
  $my_npm install
popd
pushd $my_tmp/opt/$my_name/lib/node_modules/$my_name/packages/assets
  OAUTH3_GIT_URL="https://git.oauth3.org/OAuth3/oauth3.js.git"
  git clone ${OAUTH3_GIT_URL} oauth3.org || true
  ln -s oauth3.org org.oauth3
  pushd oauth3.org
    git remote set-url origin ${OAUTH3_GIT_URL}
    git checkout $my_azp_oauth3_ver
    #git pull
  popd

  mkdir -p jquery.com
  ln -s jquery.com com.jquery
  pushd jquery.com
    http_get 'https://code.jquery.com/jquery-3.1.1.js' jquery-3.1.1.js
  popd

  mkdir -p google.com
  ln -s google.com com.google
  pushd google.com
    http_get 'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js' angular.1.6.2.min.js
  popd

  mkdir -p well-known
  ln -s well-known .well-known
  pushd well-known
    ln -snf ../oauth3.org/well-known/oauth3 ./oauth3
  popd
  echo "installed dependencies"
popd



#
# System Service
#
source ./installer/my-root.sh
echo "Pre-installation to $my_tmp complete, now installing to $my_root/ ..."
set +e
if type -p tree >/dev/null 2>/dev/null; then
  #tree -I "node_modules|include|share" $my_tmp
  tree -L 6 -I "include|share|npm" $my_tmp
else
  ls $my_tmp
fi
set -e

source ./installer/my-user-my-group.sh
echo "User $my_user Group $my_group"

source ./installer/install-system-service.sh

$sudo_cmd chown -R $my_user:$my_group $my_tmp/*
$sudo_cmd chown root:root $my_tmp/*
$sudo_cmd chown root:root $my_tmp
$sudo_cmd chmod 0755 $my_tmp
# don't change permissions on /, /etc, etc
$sudo_cmd rsync -a --ignore-existing $my_tmp/ $my_root/
$sudo_cmd rsync -a --ignore-existing $my_app_dist/etc/$my_name/$my_name.yml $my_root/etc/$my_name/$my_name.yml

# Change to admin perms
$sudo_cmd chown -R $my_user:$my_group $my_root/opt/$my_name
$sudo_cmd chown -R $my_user:$my_group $my_root/var/www $my_root/srv/www

# make sure the files are all read/write for the owner and group, and then set
# the setuid and setgid bits so that any files/directories created inside these
# directories have the same owner and group.
$sudo_cmd chmod -R ug+rwX $my_root/opt/$my_name
find $my_root/opt/$my_name -type d -exec $sudo_cmd chmod ug+s {} \;



echo ""
echo "$my_name installation complete!"
echo ""
echo ""
echo "Update the config at: /etc/$my_name/$my_name.yml"
echo ""
echo "Unistall: rm -rf /srv/$my_name/ /var/$my_name/ /etc/$my_name/ /opt/$my_name/ /var/log/$my_name/ /etc/tmpfiles.d/$my_name.conf /etc/systemd/system/$my_name.service /etc/ssl/$my_name"
