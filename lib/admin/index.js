var adminDomains = [
  'localhost.alpha.daplie.me'
, 'localhost.admin.daplie.me'
, 'alpha.localhost.daplie.me'
, 'admin.localhost.daplie.me'
, 'localhost.daplie.invalid'
];
module.exports.adminDomains = adminDomains;

module.exports.create = function (deps, conf) {
  'use strict';

  var path = require('path');
  var express = require('express');
  var app = express();

  var apis = require('./apis').create(deps, conf);
  app.use('/api/goldilocks@daplie.com', apis);
  app.use('/api/com.daplie.goldilocks', apis);

  // Serve the static assets for the UI (even though it probably won't be used very
  // often since it only works on localhost domains). Note that we are using the default
  // .well-known directory from the oauth3 library even though it indicates we have
  // capabilities we don't support because it's simpler and it's unlikely anything will
  // actually use it to determine our API (it is needed to log into the web page).
  app.use('/.well-known', express.static(path.join(__dirname, '../../packages/assets/well-known')));
  app.use('/assets',      express.static(path.join(__dirname, '../../packages/assets')));
  app.use('/',            express.static(path.join(__dirname, '../../admin/public')));

  return require('http').createServer(app);
};
