'use strict';

function bindTcpAndRelease(port, cb) {
  var server = require('net').createServer();
  server.on('error', function (e) {
    cb(e);
  });
  server.listen(port, function () {
    server.close();
    cb();
  });
}

function checkTcpPorts(cb) {
  var bound = {};
  var failed = {};

  bindTcpAndRelease(80, function (e) {
    if (e) {
      failed[80] = e;
      //console.log(e.code);
      //console.log(e.message);
    } else {
      bound['80'] = true;
    }

    bindTcpAndRelease(443, function (e) {
      if (e) {
        failed[443] = e;
      } else {
        bound['443'] = true;
      }

      if (bound['80'] && bound['443']) {
        cb(null, bound);
        return;
      }

      console.warn("default ports 80 and 443 are not available, trying 8443");

      bindTcpAndRelease(8443, function (e) {
        if (e) {
          failed[8443] = e;
        } else {
          bound['8443'] = true;
        }

        cb(failed, bound);
      });
    });
  });
}

module.exports.checkTcpPorts = checkTcpPorts;
