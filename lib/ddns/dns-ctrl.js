'use strict';

module.exports.create = function (deps, conf, utils) {
  function dnsType(addr) {
    if (/^\d+\.\d+\.\d+\.\d+$/.test(addr)) {
      return 'A';
    }
    if (-1 !== addr.indexOf(':') && /^[a-f:\.\d]+$/i.test(addr)) {
      return 'AAAA';
    }
  }

  async function setDeviceAddress(session, addr, domains) {
    var directives = await deps.OAUTH3.discover(session.token.aud);

    // Set the address of the device to our public address.
    await deps.request({
      url: deps.OAUTH3.url.normalize(directives.api)+'/api/com.daplie.domains/acl/devices/' + conf.device.hostname
    , method: 'POST'
    , headers: {
        'Authorization': 'Bearer ' + session.refresh_token
      , 'Accept': 'application/json; charset=utf-8'
      }
    , json: {
        addresses: [
          { value: addr, type:  dnsType(addr) }
        ]
      }
    });

    // Then update all of the records attached to our hostname, first removing the old records
    // to remove the reference to the old address, then creating new records for the same domains
    // using our new address.
    var allDns = await deps.OAUTH3.api(directives.api, {session: session, api: 'dns.list'});
    var ourDns = allDns.filter(function (record) {
      if (record.device !== conf.device.hostname) {
        return false;
      }
      if ([ 'A', 'AAAA' ].indexOf(record.type) < 0) {
        return false;
      }
      return domains.indexOf(record.host) !== -1;
    });

    // Of all the DNS records referring to our device and the current list of domains determine
    // which domains have records with outdated address, and which ones we can just leave be
    // without updating them.
    var badAddrDomains = ourDns.filter(function (record) {
      return record.value !== addr;
    }).map(record => record.host);
    var goodAddrDomains = ourDns.filter(function (record) {
      return record.value === addr && badAddrDomains.indexOf(record.host) < 0;
    }).map(record => record.host);
    var requiredUpdates = domains.filter(function (domain) {
      return goodAddrDomains.indexOf(domain) < 0;
    });

    var oldDns = await utils.splitDomains(directives.api, badAddrDomains);
    var common = {
      api: 'devices.detach'
    , session: session
    , device: conf.device.hostname
    };
    await deps.PromiseA.all(oldDns.map(function (record) {
      return deps.OAUTH3.api(directives.api, Object.assign({}, common, record));
    }));
    if (conf.debug && badAddrDomains.length) {
      console.log('removed bad DNS records for ' + badAddrDomains.join(', '));
    }

    var newDns = await utils.splitDomains(directives.api, requiredUpdates);
    common = {
      api: 'devices.attach'
    , session: session
    , device: conf.device.hostname
    , ip: addr
    , ttl: 300
    };
    await deps.PromiseA.all(newDns.map(function (record) {
      return deps.OAUTH3.api(directives.api, Object.assign({}, common, record));
    }));
    if (conf.debug && requiredUpdates.length) {
      console.log('set new DNS records for ' + requiredUpdates.join(', '));
    }
  }

  async function getDeviceAddresses(session) {
    var directives = await deps.OAUTH3.discover(session.token.aud);

    var result = await deps.request({
      url: deps.OAUTH3.url.normalize(directives.api)+'/api/org.oauth3.dns/acl/devices'
    , method: 'GET'
    , headers: {
        'Authorization': 'Bearer ' + session.refresh_token
      , 'Accept': 'application/json; charset=utf-8'
      }
    , json: true
    });

    if (!result.body) {
      throw new Error('No response body in request for device addresses');
    }
    if (result.body.error) {
      throw Object.assign(new Error('error getting device list'), result.body.error);
    }

    var dev = result.body.devices.filter(function (dev) {
      return dev.name === conf.device.hostname;
    })[0];
    return (dev || {}).addresses || [];
  }

  async function removeDomains(session, domains) {
    var directives = await deps.OAUTH3.discover(session.token.aud);

    var oldDns = await utils.splitDomains(directives.api, domains);
    var common = {
      api: 'devices.detach'
    , session: session
    , device: conf.device.hostname
    };
    await deps.PromiseA.all(oldDns.map(function (record) {
      return deps.OAUTH3.api(directives.api, Object.assign({}, common, record));
    }));
  }

  return {
    getDeviceAddresses
  , setDeviceAddress
  , removeDomains
  };
};
