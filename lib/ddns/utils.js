'use strict';

module.exports.create = function (deps, conf) {

  async function getSession(id) {
    var session = await deps.storage.tokens.get(id);
    if (!session) {
      throw new Error('no user token with ID "' + id + '"');
    }
    return session;
  }

  function iterateAllModules(action, curConf) {
    curConf = curConf || conf;
    var promises = [];

    curConf.domains.forEach(function (dom) {
      if (!dom.modules || !Array.isArray(dom.modules.ddns) || !dom.modules.ddns.length) {
        return null;
      }

      // For the time being all of our things should only be tried once (regardless if it succeeded)
      // TODO: revisit this behavior when we support multiple ways of setting records, and/or
      // if we want to allow later modules to run if early modules fail.
      promises.push(dom.modules.ddns.reduce(function (prom, mod) {
        if (prom) { return prom; }
        return action(mod, dom.names);
      }, null));
    });

    curConf.ddns.modules.forEach(function (mod) {
      promises.push(action(mod, mod.domains));
    });

    return Promise.all(promises.filter(Boolean));
  }

  var tldCache = {};
  async function updateTldCache(provider) {
    var reqObj = {
      url: deps.OAUTH3.url.normalize(provider) + '/api/com.daplie.domains/prices'
    , method: 'GET'
    , json: true
    };

    var resp = await deps.OAUTH3.request(reqObj);
    var tldObj = {};
    resp.data.forEach(function (tldInfo) {
      if (tldInfo.enabled) {
        tldObj[tldInfo.tld] = true;
      }
    });

    tldCache[provider] = {
      time: Date.now()
    , tlds: tldObj
    };
    return tldObj;
  }
  async function getTlds(provider) {
    // If we've never cached the results we need to return the promise that will fetch the result,
    // otherwise we can return the cached value. If the cached value has "expired", we can still
    // return the cached value we just want to update the cache in parellel (making sure we only
    // update once).
    if (!tldCache[provider]) {
      tldCache[provider] = {
        updating: true
      , tlds: updateTldCache(provider)
      };
    }
    if (!tldCache[provider].updating && Date.now() - tldCache[provider].time > 24 * 60 * 60 * 1000) {
      tldCache[provider].updating = true;
      updateTldCache(provider);
    }

    return tldCache[provider].tlds;
  }

  async function splitDomains(provider, domains) {
    var tlds = await getTlds(provider);
    return domains.map(function (domain) {
      var split = domain.split('.');
      var tldSegCnt = tlds[split.slice(-2).join('.')] ? 2 : 1;

      // Currently assuming that the sld can't contain dots, and that the tld can have at
      // most one dot. Not 100% sure this is a valid assumption, but exceptions should be
      // rare even if the assumption isn't valid.
      return {
        tld: split.slice(-tldSegCnt).join('.')
      , sld: split.slice(-tldSegCnt - 1, -tldSegCnt).join('.')
      , sub: split.slice(0, -tldSegCnt - 1).join('.')
      };
    });
  }

  return {
    getSession
  , iterateAllModules
  , getTlds
  , splitDomains
  };
};
