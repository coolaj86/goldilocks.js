'use strict';

module.exports.match = function (pattern, domainname) {
  // Everything matches '*'
  if (pattern === '*') {
    return true;
  }

  if (/^\*./.test(pattern)) {
    // get rid of the leading "*." to more easily check the servername against it
    pattern = pattern.slice(2);
    return pattern === domainname.slice(-pattern.length);
  }

  // pattern doesn't contains any wildcards, so exact match is required
  return pattern === domainname;
};

module.exports.separatePort = function (fullHost) {
  var match = /^(.*?)(:\d+)?$/.exec(fullHost);

  if (match[2]) {
    match[2] = match[2].replace(':', '');
  }

  return {
    host: match[1]
  , port: match[2]
  };
};
