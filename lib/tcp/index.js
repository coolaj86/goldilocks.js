'use strict';

module.exports.create = function (deps, config) {
  console.log('config', config);

  var listeners = require('../servers').listeners.tcp;
  var domainUtils = require('../domain-utils');
  var modules;

  var addrProperties = [
    'remoteAddress'
  , 'remotePort'
  , 'remoteFamily'
  , 'localAddress'
  , 'localPort'
  , 'localFamily'
  ];

  function nameMatchesDomains(name, domainList) {
    return domainList.some(function (pattern) {
      return domainUtils.match(pattern, name);
    });
  }

  function proxy(mod, conn, opts) {
    // First thing we need to add to the connection options is where to proxy the connection to
    var newConnOpts = domainUtils.separatePort(mod.address || '');
    newConnOpts.port = newConnOpts.port || mod.port;
    newConnOpts.host = newConnOpts.host || mod.host || 'localhost';

    // Then we add all of the connection address information. We need to prefix all of the
    // properties with '_' so we can provide the information to any connection `createConnection`
    // implementation but not have the default implementation try to bind the same local port.
    addrProperties.forEach(function (name) {
      newConnOpts['_' + name] = opts[name] || opts['_'+name] || conn[name] || conn['_'+name];
    });

    modules.proxy(conn, newConnOpts);
    return true;
  }

  function checkTcpProxy(conn, opts) {
    var proxied = false;

    // TCP Proxying (ie routing based on domain name [vs local port]) only works for
    // TLS wrapped connections, so if the opts don't give us a servername or don't tell us
    // this is the decrypted side of a TLS connection we can't handle it here.
    if (!opts.servername || !opts.encrypted) { return proxied; }

    proxied = config.domains.some(function (dom) {
      if (!dom.modules || !Array.isArray(dom.modules.tcp)) { return false; }
      if (!nameMatchesDomains(opts.servername, dom.names)) { return false; }

      return dom.modules.tcp.some(function (mod) {
        if (mod.type !== 'proxy') { return false; }

        return proxy(mod, conn, opts);
      });
    });

    proxied = proxied || config.tcp.modules.some(function (mod) {
      if (mod.type !== 'proxy') { return false; }
      if (!nameMatchesDomains(opts.servername, mod.domains)) { return false; }

      return proxy(mod, conn, opts);
    });

    return proxied;
  }

  function checkTcpForward(conn, opts) {
    // TCP forwarding (ie routing connections based on local port) requires the local port
    if (!conn.localPort) { return false; }

    return config.tcp.modules.some(function (mod) {
      if (mod.type !== 'forward')                { return false; }
      if (mod.ports.indexOf(conn.localPort) < 0) { return false; }

      return proxy(mod, conn, opts);
    });
  }

  // opts = { servername, encrypted, peek, data, remoteAddress, remotePort }
  function peek(conn, firstChunk, opts) {
    opts.firstChunk = firstChunk;
    conn.__opts = opts;
    // TODO port/service-based routing can do here

    // TLS byte 1 is handshake and byte 6 is client hello
    if (0x16 === firstChunk[0]/* && 0x01 === firstChunk[5]*/) {
      modules.tls.emit('connection', conn);
      return;
    }

    // This doesn't work with TLS, but now that we know this isn't a TLS connection we can
    // unshift the first chunk back onto the connection for future use. The unshift should
    // happen after any listeners are attached to it but before any new data comes in.
    if (!opts.hyperPeek) {
      process.nextTick(function () {
        conn.unshift(firstChunk);
      });
    }

    // Connection is not TLS, check for HTTP next.
    if (firstChunk[0] > 32 && firstChunk[0] < 127) {
      var firstStr = firstChunk.toString();
      if (/HTTP\//i.test(firstStr)) {
        modules.http.emit('connection', conn);
        return;
      }
    }

    console.warn('failed to identify protocol from first chunk', firstChunk);
    conn.destroy();
  }
  function tcpHandler(conn, opts) {
    function getProp(name) {
      return opts[name] || opts['_'+name] || conn[name] || conn['_'+name];
    }
    opts = opts || {};
    var logName = getProp('remoteAddress') + ':' + getProp('remotePort') + ' -> ' +
                  getProp('localAddress')  + ':' + getProp('localPort');
    console.log('[tcpHandler]', logName, 'connection started - encrypted: ' + (opts.encrypted || false));

    var start = Date.now();
    conn.on('timeout', function () {
      console.log('[tcpHandler]', logName, 'connection timed out', (Date.now()-start)/1000);
    });
    conn.on('end', function () {
      console.log('[tcpHandler]', logName, 'connection ended', (Date.now()-start)/1000);
    });
    conn.on('close', function () {
      console.log('[tcpHandler]', logName, 'connection closed', (Date.now()-start)/1000);
    });

    if (checkTcpForward(conn, opts)) { return; }
    if (checkTcpProxy(conn, opts))   { return; }

    // XXX PEEK COMMENT XXX
    // TODO we can have our cake and eat it too
    // we can skip the need to wrap the TLS connection twice
    // because we've already peeked at the data,
    // but this needs to be handled better before we enable that
    // (because it creates new edge cases)
    if (opts.hyperPeek) {
      console.log('hyperpeek');
      peek(conn, opts.firstChunk, opts);
      return;
    }

    function onError(err) {
      console.error('[error] socket errored peeking -', err);
      conn.destroy();
    }
    conn.once('error', onError);
    conn.once('data', function (chunk) {
      conn.removeListener('error', onError);
      peek(conn, chunk, opts);
    });
  }

  process.nextTick(function () {
    modules = {};
    modules.tcpHandler = tcpHandler;
    modules.proxy = require('./proxy-conn').create(deps, config);
    modules.tls   = require('./tls').create(deps, config, modules);
    modules.http  = require('./http').create(deps, config, modules);
  });

  function updateListeners() {
    var current = listeners.list();
    var wanted = config.tcp.bind;

    if (!Array.isArray(wanted)) { wanted = []; }
    wanted = wanted.map(Number).filter((port) => port > 0 && port < 65356);

    var closeProms = current.filter(function (port) {
      return wanted.indexOf(port) < 0;
    }).map(function (port) {
      return listeners.close(port, 1000);
    });

    // We don't really need to filter here since listening on the same port with the
    // same handler function twice is basically a no-op.
    var openProms = wanted.map(function (port) {
      return listeners.add(port, tcpHandler);
    });

    return Promise.all(closeProms.concat(openProms));
  }

  var mainPort;
  function updateConf() {
    updateListeners().catch(function (err) {
      console.error('Error updating TCP listeners to match bind configuration');
      console.error(err);
    });

    var unforwarded = {};
    config.tcp.bind.forEach(function (port) {
      unforwarded[port] = true;
    });

    config.tcp.modules.forEach(function (mod) {
      if (['forward', 'proxy'].indexOf(mod.type) < 0) {
        console.warn('unknown TCP module type specified', JSON.stringify(mod));
      }
      if (mod.type !== 'forward') { return; }

      mod.ports.forEach(function (port) {
        if (!unforwarded[port]) {
          console.warn('trying to forward TCP port ' + port + ' multiple times or it is unbound');
        } else {
          delete unforwarded[port];
        }
      });
    });

    // Not really sure what we can reasonably do to prevent this. At least not without making
    // our configuration validation more complicated.
    if (!Object.keys(unforwarded).length) {
      console.warn('no bound TCP ports are not being forwarded, admin interface will be inaccessible');
    }

    // If we are listening on port 443 make that the main port we respond to mDNS queries with
    // otherwise choose the lowest number port we are bound to but not forwarding.
    if (unforwarded['443']) {
      mainPort = 443;
    } else {
      mainPort = Object.keys(unforwarded).map(Number).sort((a, b) => a - b)[0];
    }
  }
  updateConf();

  var result =  {
    updateConf
  , handler: tcpHandler
  };
  Object.defineProperty(result, 'mainPort', {enumerable: true, get: () => mainPort});

  return result;
};
