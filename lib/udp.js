'use strict';

module.exports.create = function (deps, config) {
  var listeners = require('./servers').listeners.udp;

  function packetHandler(port, msg) {
    if (!Array.isArray(config.udp.modules)) {
      return;
    }

    var socket = require('dgram').createSocket('udp4');
    config.udp.modules.forEach(function (mod) {
      if (mod.type !== 'forward') {
        // To avoid logging bad modules every time we get a UDP packet we assign a warned
        // property to the module (non-enumerable so it won't be saved to the config or
        // show up in the API).
        if (!mod.warned) {
          console.warn('found bad DNS module', mod);
          Object.defineProperty(mod, 'warned', {value: true, enumerable: false});
        }
        return;
      }
      if (mod.ports.indexOf(port) < 0) {
        return;
      }

      var dest = require('./domain-utils').separatePort(mod.address || '');
      dest.port = dest.port || mod.port;
      dest.host = dest.host || mod.host || 'localhost';
      socket.send(msg, dest.port, dest.host);
    });
  }

  function updateListeners() {
    var current = listeners.list();
    var wanted = config.udp.bind;

    if (!Array.isArray(wanted)) { wanted = []; }
    wanted = wanted.map(Number).filter((port) => port > 0 && port < 65356);

    current.forEach(function (port) {
      if (wanted.indexOf(port) < 0) {
        listeners.close(port);
      }
    });
    wanted.forEach(function (port) {
      if (current.indexOf(port) < 0) {
        listeners.add(port, packetHandler.bind(port));
      }
    });
  }

  updateListeners();
  return {
    updateConf: updateListeners
  };
};
